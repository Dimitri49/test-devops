import pytest
from main import sum, sub

def test_sum():
    assert sum(1, 1) == 2

def test_sub():
    assert sub(1, 1) == 0